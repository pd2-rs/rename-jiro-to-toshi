local JiroToToshiMod = {}
JiroToToshiMod._loc_path = ModPath .. 'loc/'

function JiroToToshiMod.get_loc_file(lang)
  if BLT and BLT.Localization then
    local lang = BLT.Localization:get_language().language
    local file = JiroToToshiMod._loc_path .. lang .. ".txt"

    if io.file_is_readable(file) then
      return file
    end
  end
  return JiroToToshiMod._loc_path .. 'en.txt'
end

function JiroToToshiMod.load_loc()
  Hooks:Add("LocalizationManagerPostInit", "jirototoshi_loc_init_id",
    function(self, ...)
      local loc_file = JiroToToshiMod.get_loc_file()
      self:load_localization_file(loc_file, false)
    end
  )
end

JiroToToshiMod.load_loc()